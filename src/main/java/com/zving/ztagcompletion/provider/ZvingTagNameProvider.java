package com.zving.ztagcompletion.provider;

import com.intellij.codeInsight.completion.XmlTagInsertHandler;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.icons.AllIcons;
import com.intellij.psi.html.HtmlTag;
import com.intellij.psi.impl.source.xml.XmlElementDescriptorProvider;
import com.intellij.psi.xml.XmlTag;
import com.intellij.xml.XmlElementDescriptor;
import com.intellij.xml.XmlTagNameProvider;
import com.zving.ztagcompletion.descriptor.CmsCatalogDescriptor;
import com.zving.ztagcompletion.descriptor.CmsContentDescriptor;
import com.zving.ztagcompletion.descriptor.ZvingTagDescriptor;
import com.zving.ztagcompletion.tag.ZvingTagLoader;
import com.zving.ztagcompletion.tag.ZvingTag;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ZvingTagNameProvider implements XmlTagNameProvider, XmlElementDescriptorProvider {

    @Override
    public void addTagNameVariants(List<LookupElement> list, @NotNull XmlTag xmlTag, String prefix) {
        if (!(xmlTag instanceof HtmlTag)) {
            return;
        }
        // 单个加载
        LookupElement cmsCatalog = LookupElementBuilder
                .create("cms:catalog")
                .withInsertHandler(XmlTagInsertHandler.INSTANCE)
                .withBoldness(true)
                .withIcon(AllIcons.Nodes.Tag)
                .withTypeText("ZCMS栏目标签");
        list.add(cmsCatalog);

        LookupElement cmsContent = LookupElementBuilder
                .create("cms:content")
                .withInsertHandler(XmlTagInsertHandler.INSTANCE)
                .withBoldness(true)
                .withIcon(AllIcons.Nodes.Tag)
                .withTypeText("ZCMS内容标签");
        list.add(cmsContent);

        // 批量加载
        ZvingTag[] tags = ZvingTagLoader.getZvingTags();
        for (ZvingTag tag : tags) {
            LookupElement element = LookupElementBuilder
                    .create(tag.tag)
                    .withInsertHandler(XmlTagInsertHandler.INSTANCE)
                    .withBoldness(true)
                    .withIcon(AllIcons.Nodes.Tag)
                    .withTypeText(tag.typeText);
            list.add(element);
        }
    }

    @Override
    public @Nullable XmlElementDescriptor getDescriptor(XmlTag xmlTag) {
        if (xmlTag.getContainingFile() == null || !xmlTag.getName().toLowerCase().contains("cms:")) {
            return null;
        }

        // 单个加载
        if (xmlTag.getName().equalsIgnoreCase("cms:catalog")) {
            return new CmsCatalogDescriptor(xmlTag.getName(), xmlTag.getContainingFile());
        } else if (xmlTag.getName().equalsIgnoreCase("cms:content")) {
            return new CmsContentDescriptor(xmlTag.getName(), xmlTag.getContainingFile());
        }

        // 批量加载
        ZvingTag[] tags = ZvingTagLoader.getZvingTags();
        for (ZvingTag tag : tags) {
            if (xmlTag.getName().equalsIgnoreCase(tag.tag)) {
                return new ZvingTagDescriptor(tag.tag, tag.declare);
            }
        }

        return null;
    }
}
