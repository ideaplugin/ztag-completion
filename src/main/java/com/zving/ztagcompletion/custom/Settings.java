package com.zving.ztagcompletion.custom;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.json.JsonFileType;
import com.intellij.notification.NotificationGroupManager;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.project.ProjectUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.zving.ztagcompletion.tag.ZvingTagLoader;
import org.apache.http.util.TextUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class Settings implements Configurable {
    private JPanel contentPane;
    private JTextField textField1;
    private JButton selectButton;
    private JButton resetButton;

    public static final String KEY_RULES_PATH = "RULES_PATH";
    public static final String KEY_GLOBAL_COMPONENTS = "WEEX_GLOBAL_COMPONENTS";

    public static final String ID = "com.zving.ztagcompletion.custom.Settings";

    @Nls
    @Override
    public String getDisplayName() {
        return "Zving Tag Completion";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return "";
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        selectButton.addActionListener(e -> {
            // Json文件选择
            FileChooserDescriptor descriptor = FileChooserDescriptorFactory
                    .createSingleFileDescriptor(JsonFileType.INSTANCE);
            // 获取自定义配置的VirtualFile实例
            VirtualFile virtualFile = FileChooser.chooseFile(descriptor,
                    ProjectManager.getInstance().getDefaultProject(),
                    null);
            // 显示文件名路径
            if (virtualFile != null && !virtualFile.isDirectory()) {
                textField1.setText(virtualFile.getCanonicalPath());
            }
        });

        // 重置
        resetButton.addActionListener(e -> textField1.setText(""));

        // 自定义标签路径回显
        textField1.setText(PropertiesComponent.getInstance().getValue(KEY_RULES_PATH, ""));

        return contentPane;
    }

    @Override
    public boolean isModified() {
        return true;
    }

    @Override
    public void apply() {
        try {
            PropertiesComponent.getInstance().setValue(KEY_RULES_PATH, textField1.getText());
            if (!TextUtils.isEmpty(textField1.getText())) {
                ZvingTagLoader.mergeCustom(textField1.getText());
                NotificationGroupManager.getInstance().getNotificationGroup("Zving tag completion")
                        .createNotification("自定义标签加载成功", NotificationType.INFORMATION)
                        .notify(ProjectUtil.guessCurrentProject(selectButton));
            } else {
                ZvingTagLoader.reset();
            }
        } catch (Exception e) {
            NotificationGroupManager.getInstance().getNotificationGroup("Zving tag completion")
                    .createNotification(e.toString(), NotificationType.ERROR)
                    .notify(ProjectUtil.guessCurrentProject(selectButton));
        }
        savePaths();
    }

    private void savePaths() {
        PropertiesComponent.getInstance().setValue(KEY_GLOBAL_COMPONENTS, textField1.getText());
    }

}
