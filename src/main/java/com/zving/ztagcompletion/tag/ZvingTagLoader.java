package com.zving.ztagcompletion.tag;

import com.google.gson.Gson;
import com.intellij.ide.util.PropertiesComponent;
import com.zving.ztagcompletion.custom.Settings;
import org.apache.commons.lang3.ArrayUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class ZvingTagLoader {

    private static ZvingTag[] tags = null;

    public static ZvingTag[] getZvingTags() {
        prepare();
        return tags;
    }

    public static ZvingTag getZvingTag(String tagName) {
        prepare();
        for (ZvingTag tag : tags) {
            if (tag.tag.equals(tagName)) {
                return tag;
            }
        }
        return null;
    }

    private static void loadZvingTags() {
        InputStream is = ZvingTagLoader.class.getResourceAsStream("/directives.json");
        Gson gson = new Gson();
        if (is != null) {
            tags = gson.fromJson(new InputStreamReader(is, StandardCharsets.UTF_8), ZvingTag[].class);
        }
        if (tags == null) {
            tags = new ZvingTag[0];
        }
    }

    public static void mergeCustom(String filePath) {
        InputStream is;
        try {
            is = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        Gson gson = new Gson();
        reset();
        tags = ArrayUtils.addAll(tags, gson.fromJson(new InputStreamReader(is, StandardCharsets.UTF_8), ZvingTag[].class));
        if (tags == null) {
            tags = new ZvingTag[0];
        }
    }

    public static void prepare() {
        if (tags == null) {
            loadZvingTags();
            mergeCustom(PropertiesComponent.getInstance().getValue(Settings.KEY_RULES_PATH, ""));
        }
    }

    public static void reset() {
        loadZvingTags();
    }

}
