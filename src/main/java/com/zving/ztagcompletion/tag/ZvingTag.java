package com.zving.ztagcompletion.tag;

import com.intellij.psi.PsiFile;

import java.util.concurrent.CopyOnWriteArrayList;

public class ZvingTag {
    public String tag;
    public CopyOnWriteArrayList<Attribute> attrs;
    public PsiFile declare;
    public String typeText;

    public static class Attribute {
        public String name;
        public String[] valueEnum;
        public String valueType;
    }

    @Override
    public String toString() {
        String file = "null";
        if (declare != null) {
            file = declare.getOriginalFile().getVirtualFile().getName();
        }
        return "\n{" + tag + "} : " + file;
    }
}