package com.zving.ztagcompletion.descriptor;

import com.intellij.openapi.util.NlsSafe;
import com.intellij.psi.PsiElement;
import com.intellij.xml.XmlAttributeDescriptor;
import com.intellij.xml.impl.BasicXmlAttributeDescriptor;

public class ZvingTagAttributeDescriptor extends BasicXmlAttributeDescriptor implements XmlAttributeDescriptor {

    private final String name;
    private final boolean isEnumerated;
    private final String[] enumeratedValues;

    public ZvingTagAttributeDescriptor(String name, String[] enumeratedValues) {
        this.name = name;
        this.isEnumerated = true;
        this.enumeratedValues = enumeratedValues;
    }

    public ZvingTagAttributeDescriptor(String name) {
        this.name = name;
        this.isEnumerated = false;
        this.enumeratedValues = new String[0];
    }

    @Override
    public boolean isRequired() {
        return false;
    }

    @Override
    public boolean hasIdType() {
        return false;
    }

    @Override
    public boolean hasIdRefType() {
        return false;
    }

    @Override
    public boolean isEnumerated() {
        return isEnumerated;
    }

    @Override
    public PsiElement getDeclaration() {
        return null;
    }

    @Override
    public @NlsSafe String getName() {
        return name;
    }

    @Override
    public void init(PsiElement element) {

    }

    @Override
    public boolean isFixed() {
        return false;
    }

    @Override
    public String getDefaultValue() {
        return null;
    }

    @Override
    public String[] getEnumeratedValues() {
        return enumeratedValues;
    }

}
