package com.zving.ztagcompletion.descriptor;

import com.intellij.openapi.util.NlsSafe;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.xml.XmlAttributeDescriptor;
import com.intellij.xml.XmlElementDescriptor;
import com.intellij.xml.XmlElementsGroup;
import com.intellij.xml.XmlNSDescriptor;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.Nullable;

public class CmsCatalogDescriptor implements XmlElementDescriptor {

    protected final String name;
    private final PsiFile file;

    public CmsCatalogDescriptor(String name, PsiFile file) {
        this.name = name;
        this.file = file;
    }

    @Override
    public @NonNls String getQualifiedName() {
        return name;
    }

    @Override
    public @NonNls String getDefaultName() {
        return name;
    }

    @Override
    public XmlElementDescriptor[] getElementsDescriptors(XmlTag context) {
        return EMPTY_ARRAY;
    }

    @Override
    public @Nullable XmlElementDescriptor getElementDescriptor(XmlTag childTag, XmlTag contextTag) {
        return null;
    }

    @Override
    public XmlAttributeDescriptor[] getAttributesDescriptors(@Nullable XmlTag context) {
        XmlAttributeDescriptor[] customAttributes = new XmlAttributeDescriptor[12];
        customAttributes[0] = new ZvingTagAttributeDescriptor("begin");
        customAttributes[1] = new ZvingTagAttributeDescriptor("page");
        customAttributes[2] = new ZvingTagAttributeDescriptor("pagesize");
        customAttributes[3] = new ZvingTagAttributeDescriptor("count");
        customAttributes[4] = new ZvingTagAttributeDescriptor("condition");
        customAttributes[5] = new ZvingTagAttributeDescriptor("loadextend");
        customAttributes[6] = new ZvingTagAttributeDescriptor("name");
        customAttributes[7] = new ZvingTagAttributeDescriptor("id");
        customAttributes[8] = new ZvingTagAttributeDescriptor("innercode");
        customAttributes[9] = new ZvingTagAttributeDescriptor("alias");
        customAttributes[10] = new ZvingTagAttributeDescriptor("type");
        customAttributes[11] = new ZvingTagAttributeDescriptor("level");
        return customAttributes;
    }

    @Override
    public @Nullable XmlAttributeDescriptor getAttributeDescriptor(@NonNls String attributeName, @Nullable XmlTag context) {
        return ContainerUtil.find(getAttributesDescriptors(context), descriptor1 -> attributeName.equals(descriptor1.getName()));
    }

    @Override
    public @Nullable XmlAttributeDescriptor getAttributeDescriptor(XmlAttribute attribute) {
        return getAttributeDescriptor(attribute.getName(), attribute.getParent());
    }

    @Override
    public @Nullable XmlNSDescriptor getNSDescriptor() {
        return null;
    }

    @Override
    public @Nullable XmlElementsGroup getTopGroup() {
        return null;
    }

    @Override
    public int getContentType() {
        return CONTENT_TYPE_ANY;
    }

    @Override
    public @Nullable String getDefaultValue() {
        return null;
    }

    @Override
    public PsiElement getDeclaration() {
        return file;
    }

    @Override
    public @NonNls String getName(PsiElement context) {
        return getName();
    }

    @Override
    public @NlsSafe String getName() {
        return name;
    }

    @Override
    public void init(PsiElement element) {

    }
}
