package com.zving.ztagcompletion.descriptor;

import com.intellij.openapi.util.NlsSafe;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.xml.XmlAttributeDescriptor;
import com.intellij.xml.XmlElementDescriptor;
import com.intellij.xml.XmlElementsGroup;
import com.intellij.xml.XmlNSDescriptor;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.Nullable;

public class CmsContentDescriptor implements XmlElementDescriptor {


    protected final String name;
    private final PsiFile file;

    public CmsContentDescriptor(String name, PsiFile file) {
        this.name = name;
        this.file = file;
    }

    @Override
    public @NonNls String getQualifiedName() {
        return name;
    }

    @Override
    public @NonNls String getDefaultName() {
        return name;
    }

    @Override
    public XmlElementDescriptor[] getElementsDescriptors(XmlTag context) {
        return EMPTY_ARRAY;
    }

    @Override
    public @Nullable XmlElementDescriptor getElementDescriptor(XmlTag childTag, XmlTag contextTag) {
        return null;
    }

    @Override
    public XmlAttributeDescriptor[] getAttributesDescriptors(@Nullable XmlTag context) {
        XmlAttributeDescriptor[] customAttributes = new XmlAttributeDescriptor[32];
        customAttributes[0] = new ZvingTagAttributeDescriptor("preview", new String[]{"true", "false"});
        customAttributes[1] = new ZvingTagAttributeDescriptor("alt");
        customAttributes[2] = new ZvingTagAttributeDescriptor("begin");
        customAttributes[3] = new ZvingTagAttributeDescriptor("page");
        customAttributes[4] = new ZvingTagAttributeDescriptor("pageSize");
        customAttributes[5] = new ZvingTagAttributeDescriptor("count");
        customAttributes[6] = new ZvingTagAttributeDescriptor("condition");
        customAttributes[7] = new ZvingTagAttributeDescriptor("loadExtend");
        customAttributes[8] = new ZvingTagAttributeDescriptor("loadContent");
        customAttributes[9] = new ZvingTagAttributeDescriptor("loadProps");
        customAttributes[10] = new ZvingTagAttributeDescriptor("hasLogo");
        customAttributes[11] = new ZvingTagAttributeDescriptor("siteID");
        customAttributes[12] = new ZvingTagAttributeDescriptor("catalogID");
        customAttributes[13] = new ZvingTagAttributeDescriptor("catalogAlias");
        customAttributes[14] = new ZvingTagAttributeDescriptor("catalog");
        customAttributes[15] = new ZvingTagAttributeDescriptor("hasAttribute");
        customAttributes[16] = new ZvingTagAttributeDescriptor("platformAttribute");
        customAttributes[17] = new ZvingTagAttributeDescriptor("noAttribute");
        customAttributes[18] = new ZvingTagAttributeDescriptor("contenttype");
        customAttributes[19] = new ZvingTagAttributeDescriptor("minWeight");
        customAttributes[20] = new ZvingTagAttributeDescriptor("maxWeight");
        customAttributes[21] = new ZvingTagAttributeDescriptor("keyword");
        customAttributes[22] = new ZvingTagAttributeDescriptor("orderby");
        customAttributes[23] = new ZvingTagAttributeDescriptor("flags");
        customAttributes[24] = new ZvingTagAttributeDescriptor("name");
        customAttributes[25] = new ZvingTagAttributeDescriptor("id");
        customAttributes[26] = new ZvingTagAttributeDescriptor("publishtime");
        customAttributes[27] = new ZvingTagAttributeDescriptor("contentType");
        customAttributes[28] = new ZvingTagAttributeDescriptor("type");
        customAttributes[29] = new ZvingTagAttributeDescriptor("level");
        customAttributes[30] = new ZvingTagAttributeDescriptor("contentType");
        customAttributes[31] = new ZvingTagAttributeDescriptor("loadExtendDAO");
        return customAttributes;
    }

    @Override
    public @Nullable XmlAttributeDescriptor getAttributeDescriptor(@NonNls String attributeName, @Nullable XmlTag context) {
        return ContainerUtil.find(getAttributesDescriptors(context), descriptor1 -> attributeName.equals(descriptor1.getName()));
    }

    @Override
    public @Nullable XmlAttributeDescriptor getAttributeDescriptor(XmlAttribute attribute) {
        return getAttributeDescriptor(attribute.getName(), attribute.getParent());
    }

    @Override
    public @Nullable XmlNSDescriptor getNSDescriptor() {
        return null;
    }

    @Override
    public @Nullable XmlElementsGroup getTopGroup() {
        return null;
    }

    @Override
    public int getContentType() {
        return CONTENT_TYPE_ANY;
    }

    @Override
    public @Nullable String getDefaultValue() {
        return null;
    }

    @Override
    public PsiElement getDeclaration() {
        return file;
    }

    @Override
    public @NonNls String getName(PsiElement context) {
        return getName();
    }

    @Override
    public @NlsSafe String getName() {
        return name;
    }

    @Override
    public void init(PsiElement element) {

    }

}
