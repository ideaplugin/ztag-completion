package com.zving.ztagcompletion.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.ui.Messages;

public class AboutAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {
        Messages.showMessageDialog("泽元模版标签补全工具（版本号1.0）\r\n\r\ncopyright © 2007-2024 zving.com. all rights reserved 北京泽元迅长软件有限公司", "关于", Messages.getInformationIcon());
    }
}
